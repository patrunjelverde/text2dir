﻿### As an Administrator, you can set the      ###
### execution policy by typing this into your ###
### PowerShell window:                        ###
#################################################
# Set-ExecutionPolicy RemoteSigned

# Param
 param (
    # Available modes: unicode, hash
    [string]$m = "unicode",
    [string]$f = "data.txt",
    [string]$prefix = "output\"
 )

# Variable declarations
$data_dir = $pwd;
$data_file = $f;
$data_path = "$data_dir\$data_file";

$hash = @{}
############################################
# Define forbidden characters ~#%*{}:<>?/+|"
############################################
$hash.'~' = ''
$hash.'#' = ''
$hash.'%' = ''
$hash.'*' = ''
$hash.'{' = ''
$hash.'}' = ''
$hash.':' = ''
$hash.'<' = ''
$hash.'>' = ''
$hash.'?' = ''
$hash.'/' = ''
$hash.'+' = ''
$hash.'|' = ''
$hash.'"' = ''
$hash.'@' = ''
$hash.'^' = ''
$hash.'$' = ''
$hash.'!' = ''
$hash.'&' = ''
#debug
#$hash.'d' = ''
############################################
# End define
############################################

$data_content = @();

# Function declarations
function debug_msg ($msg) {
    Write-host $msg
    write-host $data_content.count total lines read from file;
}

function get_data ($data_file_path) {
    $data = Get-Content $data_file_path;
    write-host $data.count total lines read from file;
    $data;
}
function set_config () {

}

function create_dir ($dir_names) {
    ForEach ($dir_name in $dir_names) {
        # mkdir "$prefix$dir_name";
        $target_dir = "$data_dir\$prefix$dir_name";
        if(!(Test-Path -Path $target_dir )){
            New-Item -ItemType directory -Path $target_dir;
            Write-Host CREATED DIRECTORY: $target_dir;
        }
        else {
            Write-Host SKIPPED DIRECTORY: $target_dir;
        }
    }
}

function remove_special_characters ($data, $method) {

   switch ($method) {
      "unicode" {
         # For reference http://www.asciitable.com/
         $results = @( ForEach ( $line in $data ) { $line -replace '[^\x20-\x21\x30-\x39\x5F\x41-\x5A\x61-\x7A]+', ''; } );
         $results;
         break
      }
      "hash" {
         ForEach ( $key in $hash.Keys ) { $data = $data.Replace($key, $hash.$key); }
         $data;
         break
      }
      default {"Something happened: Invalid method ($method)."; break}
   }
}

function main {
    if([System.IO.File]::Exists($data_path)){
        $data_content = get_data $data_path;
        $data_content_clean = @(remove_special_characters $data_content $m);
        create_dir($data_content_clean);
    }
    else {
        Write-Host `"$data_path`" : ...not found!;
    }
}

#Run main
main;